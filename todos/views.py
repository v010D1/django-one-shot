from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "list_detail": detail,
    }

    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_edit(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=post)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.id)
    else:
        form = TodoListForm(instance=post)
    context = {
        "todo_list_object": post,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    deleted_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        deleted_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", item.list.id)
    else:
        form = TodoItemForm()
        context = {
            "form": form,
        }
    return render(request, "todos/create.html", context)


def todo_item_update(request, id):
    updated_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=updated_item)
        if form.is_valid():
            update = form.save()
            return redirect("todo_list_detail", id=update.id)
    else:
        form = TodoItemForm(instance=updated_item)
    context = {
        "form": form,
    }
    return render(request, "todos/itemupdate.html", context)
